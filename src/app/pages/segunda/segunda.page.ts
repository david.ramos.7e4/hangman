import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-segunda',
  templateUrl: './segunda.page.html',
  styleUrls: ['./segunda.page.scss'],
})
export class SegundaPage implements OnInit {

  dificultad: string ;
  intentos: number;
  palabras_ocultas: ["ordenador", "sudadera", "playa", "imaginación", "infancia", "programación", "iphone"]
  posicion_palabra: number; 
  palabra: string;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if(params && params.dificultad){
        // this.posicion_palabra = Math.floor(Math.random()*(this.palabras_ocultas.length));
        // this.palabra= this.palabras_ocultas[this.posicion_palabra];
        this.dificultad = params.dificultad;
      }
      if(this.dificultad == "easy"){
        this.intentos = 8;
        
      } else if((this.dificultad == "hard")){
        this.intentos = 5;
        
      } 
    });
  }

  pantallafinal() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad,
        intentos: this.intentos
      }
    }
    this.router.navigate(['tercera'], navigationExtras)
  }
  ngOnInit() {
  }
}




