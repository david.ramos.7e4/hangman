import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

 
  dificultad: string='';

  constructor(private router: Router) { }

  segunda(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    }; 
    this.router.navigate(['segunda'], navigationExtras)
    if (this.dificultad = "") {
      dificultad: this.dificultad + "easy";
    }
    this.router.navigate(['segunda'], navigationExtras)
  }


  ngOnInit() {
  }

}
