import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-tercera',
  templateUrl: './tercera.page.html',
  styleUrls: ['./tercera.page.scss'],
})
export class TerceraPage implements OnInit {
  intentos: number;
  dificultad: string;
  resultado:string;

  constructor(private route: ActivatedRoute, public router: Router) {
    this.route.queryParams.subscribe(params => {
      if(params && params.intentos && params.dificultad){
        this.intentos = params.intentos
        this.dificultad = params.dificultad
      }

      if (this.intentos > 0) {
        this.resultado = "Felicidades!!!";
      } else if (this.intentos = 0) {
        this.resultado = "La próxima será!!!";
      }
    });
  }

  volverajugar() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad,
        intentos:this.intentos
      }
    }
    this.router.navigate(['segunda'], navigationExtras)
  }
  inicio() {
    this.router.navigate(['inicio'])
  }

  ngOnInit() {
  }

}
